package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe um número do usuário
 * e imprime a fatorial desse mesmo número.
 * 
 */
public class Fatorial {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite número:");
		double numero = scanner.nextFloat();
  		double numeroant = 1;
		double fatorial = 1;
		int c = 1;
		
		while (c <= numero) {
			fatorial = c * numeroant;
			numeroant = fatorial;
			c++;
			}
		
		System.out.println("O fatorial é:");
		System.out.println(fatorial);
	}
}
