package a_basicos;

/**
 * Crie um programa que imprima todas as cartas do baralho.
 * 
 * Exemplo: 
 * 
 * Ás de Ouros
 * Ás de Espadas
 * Ás de Copas
 * Ás de Paus
 * Dois de Ouros
 * ...
 * 
 */
public class Baralho {
	public static void main(String[] args) {
		String[] ranks = {"Ás", "Dois", "Três", "Quatro"};
		String[] naipes = {"Paus", "Ouros", "Copas", "Espadas"};
		
		for(int i = 0; i < ranks.length; i++) {
			for(int j = 0 ; j < naipes.length; j++) {
				System.out.println(ranks[i] + " de " + naipes[j] );
			}
		}
	}
}
